package com.cx.submissionmoviecatalogue1.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import com.cx.submissionmoviecatalogue1.BuildConfig
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.network.NetworkApi
import com.cx.submissionmoviecatalogue1.model.MovieTodayModel
import com.cx.submissionmoviecatalogue1.utils.getDate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    2019-09-07.
 */
class ReminderService : JobService() {

    override fun onStopJob(p0: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(p0: JobParameters?): Boolean {
        getMovie()
        return true
    }

    private fun getMovie() {
        val call: Call<MovieTodayModel> = NetworkApi().makeService().getMovieRelease(
            BuildConfig.API_KEY,
            getDate(), getDate()
        )
        call.enqueue(object : Callback<MovieTodayModel> {
            override fun onFailure(call: Call<MovieTodayModel>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(
                call: Call<MovieTodayModel>,
                response: Response<MovieTodayModel>
            ) {
                if (response.isSuccessful) {
                    response.body()?.results.let {
                        if (!it.isNullOrEmpty()) {
                            for (i in it) {
                                showNotification(applicationContext, i.title!!, i.overview!!, 1)
                            }
                        }
                    }
                }
            }
        })

    }

    private fun showNotification(context: Context, title: String, message: String, notifId: Int) {
        val CHANNEL_ID = "Channel_1"
        val CHANNEL_NAME = "Job scheduler channel"

        val notificationManagerCompat =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setContentTitle(title)
            .setSmallIcon(R.drawable.ic_favorite_black_24dp)
            .setContentText(message)
            .setColor(ContextCompat.getColor(context, android.R.color.black))
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .setSound(alarmSound)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(1000, 1000, 1000, 1000, 1000)
            builder.setChannelId(CHANNEL_ID)
            notificationManagerCompat.createNotificationChannel(channel)
        }
        val notification = builder.build()
        notificationManagerCompat.notify(notifId, notification)
    }

}