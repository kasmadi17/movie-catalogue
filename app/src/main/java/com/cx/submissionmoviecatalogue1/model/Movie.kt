package com.cx.submissionmoviecatalogue1.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    2019-08-24.
 */
@Parcelize
class Movie(
    var id: Int? = 0,
    var title: String?,
    var description: String?,
    var releaseDate: String?,
    var poster: String?,
    var type: Int = 0
) : Parcelable