package com.cx.submissionmoviecatalogue1.model

import com.google.gson.annotations.SerializedName

data class MovieTodayModel(

    @field:SerializedName("page")
    val page: Int? = null,

    @field:SerializedName("total_pages")
    val totalPages: Int? = null,

    @field:SerializedName("results")
    val results: List<ResultsMovieToday>? = null,

    @field:SerializedName("total_results")
    val totalResults: Int? = null
)