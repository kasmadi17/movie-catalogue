package com.cx.submissionmoviecatalogue1.model

import com.google.gson.annotations.SerializedName

data class MovieModel(

	@field:SerializedName("page")
	val page: Int? = null,

	@field:SerializedName("total_pages")
	val totalPages: Int? = null,

	@field:SerializedName("results")
	val results: List<ResultsMovie>? = null,

	@field:SerializedName("total_results")
	val totalResults: Int? = null
)