package com.cx.submissionmoviecatalogue1.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.view.View
import java.util.*


/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    30/06/19.
 */

fun setLanguage(context: Context, language: String) {
    val locale = Locale(language)
    Locale.setDefault(locale)
    val res = context.resources
    val config = Configuration(res.configuration)
    config.setLocale(locale)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        context.resources.updateConfiguration(config, context.resources.displayMetrics)
    } else {
        context.createConfigurationContext(config)
    }
}

fun View.visible(){
    visibility=View.VISIBLE
}

fun View.invisible(){
    visibility = View.INVISIBLE
}