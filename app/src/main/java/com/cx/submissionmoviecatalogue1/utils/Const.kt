package com.cx.submissionmoviecatalogue1.utils

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    26/06/19.
 */
object Const {
    const val MOVIE = "movie"
    const val TV_SHOW = "tv_show"
    const val LOCALE_IN = "id"
    const val LOCALE_ENGLISH = "en_US"
    const val DEFAULT_LOCALE = "DEFAULT"
    const val BASE_IMG_URL = "https://image.tmdb.org/t/p/w185/"
    const val IMG_URL_W342 = "https://image.tmdb.org/t/p/w342/"
    const val DAILY_REMINDER = "daily_reminder"
    const val RELEASE_REMINDER = "release_reminder"
}