package com.cx.submissionmoviecatalogue1.ui.search

import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.model.ResultsMovie
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.ui.movie.MovieAdapter
import com.cx.submissionmoviecatalogue1.ui.movie.MovieViewModel
import com.cx.submissionmoviecatalogue1.ui.movie.moviedetail.DetailActivity
import com.cx.submissionmoviecatalogue1.ui.tvshow.TvShowAdapter
import com.cx.submissionmoviecatalogue1.ui.tvshow.TvShowViewModel
import com.cx.submissionmoviecatalogue1.ui.tvshow.tvshowdetail.DetailTvShowActivity
import com.cx.submissionmoviecatalogue1.utils.Const.MOVIE
import com.cx.submissionmoviecatalogue1.utils.Const.TV_SHOW
import kotlinx.android.synthetic.main.list.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivity

class SearchActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var movieViewModel: MovieViewModel
    private lateinit var tvShowViewModel: TvShowViewModel
    private lateinit var adapter: MovieAdapter
    private lateinit var adapterTvShow: TvShowAdapter
    private var type = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        type = intent.getIntExtra("type", -1)
        info("intent $type")
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.search)?.actionView as SearchView
        searchView.setSearchableInfo(
            searchManager
                .getSearchableInfo(componentName)
        )
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.queryHint = getString(R.string.movie_title)
        searchView.isFocusable = true
        searchView.isIconified = false
        searchView.requestFocusFromTouch()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNullOrEmpty()) {
                    if (type == 0) {
                        movieViewModel.getMovie().observe(this@SearchActivity, getMovie)
                        movieViewModel.searchMovie("en_US", query)
                    } else {
                        tvShowViewModel.getTvShow().observe(this@SearchActivity, getTvShow)
                        tvShowViewModel.searchTvShow("en_US", query)
                    }

                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!newText.isNullOrEmpty()) {
                    if (newText.length > 3) {
                        if (type == 0) {
                            movieViewModel.getMovie().observe(this@SearchActivity, getMovie)
                            movieViewModel.searchMovie("en_US", newText)
                        } else {
                            tvShowViewModel.getTvShow().observe(this@SearchActivity, getTvShow)
                            tvShowViewModel.searchTvShow("en_US", newText)
                        }
                    }
                }
                return false
            }

        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        return if (id == R.id.search) {
            true
        } else super.onOptionsItemSelected(item)
    }

    private val getMovie = Observer<ArrayList<ResultsMovie>> {
        if (it != null) {
            adapter.setData(it)
        }
    }

    private val getTvShow = Observer<ArrayList<ResultsTvShow>> {
        if (it != null) {
            adapterTvShow.setData(it)
        }
    }

    private fun init() {
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        rcView.layoutManager = llm
        if (type == 0) {
            movieViewModel = ViewModelProviders.of(this).get(MovieViewModel::class.java)
            adapter = MovieAdapter {
                startActivity<DetailActivity>(MOVIE to it)
            }
            rcView.adapter = adapter
        } else {
            tvShowViewModel = ViewModelProviders.of(this).get(TvShowViewModel::class.java)
            adapterTvShow = TvShowAdapter {
                startActivity<DetailTvShowActivity>(TV_SHOW to it)
            }

            rcView.adapter = adapterTvShow
        }
    }
}
