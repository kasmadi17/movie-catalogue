package com.cx.submissionmoviecatalogue1.ui.tvshow.tvshowdetail

import android.content.ContentValues
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.CONTENT_URI
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.DESCRIPTION
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.IDS
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.POSTER
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.RELEASE_DATE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TITLE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TYPE
import com.cx.submissionmoviecatalogue1.model.Movie
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.utils.Const.IMG_URL_W342
import com.cx.submissionmoviecatalogue1.utils.Const.TV_SHOW
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_tv_show_content.*
import kotlinx.android.synthetic.main.item_tv_show.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class DetailTvShowActivity : AppCompatActivity(), AnkoLogger {

    private var movie: ResultsTvShow? = null
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var favoriteMovie: Movie? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_tv_show)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.favorite -> {
                if (isFavorite) delete() else insert()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun init() {
        setData()
        checkFavorite()
        setFavorite()
    }

    private fun setData() {
        movie = intent.getParcelableExtra<ResultsTvShow>(TV_SHOW).apply {
            tvTitleShow.text = name
            tvReleaseShow.text = firstAirDate
            posterPath.let { Picasso.get().load("$IMG_URL_W342 $it").into(imgPoster) }
            if (overview.isNullOrBlank()) {
                tvDescShow.text = getString(R.string.overview_not_available)
            } else {
                tvDescShow.text = overview
            }
            favoriteMovie = Movie(id, name, overview, firstAirDate, posterPath, 1)
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menuItem?.getItem(0)?.icon =
                ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

    private fun checkFavorite() {
        doAsync {
            val uri = Uri.parse("$CONTENT_URI/${favoriteMovie?.id}")
            val cursor = contentResolver.query(uri, null, null, null, null)
            uiThread {
                if (cursor.moveToNext()) {
                    isFavorite = cursor != null
                }
            }
        }
    }

    private fun insert() {
        doAsync {
            val values = ContentValues()
            values.put(IDS, favoriteMovie?.id)
            values.put(TITLE, favoriteMovie?.title)
            values.put(DESCRIPTION, favoriteMovie?.description)
            values.put(RELEASE_DATE, favoriteMovie?.releaseDate)
            values.put(POSTER, favoriteMovie?.poster)
            values.put(TYPE, favoriteMovie?.type)
            contentResolver.insert(CONTENT_URI, values)
            uiThread {
                toast(getString(R.string.msg_save_fav))
            }
        }
    }

    private fun delete() {
        doAsync {
            val uri = Uri.parse("$CONTENT_URI/${favoriteMovie?.id}")
            contentResolver.delete(uri, null, null)
            uiThread {
                toast(getString(R.string.msg_delete_fav))
            }
        }
    }
}
