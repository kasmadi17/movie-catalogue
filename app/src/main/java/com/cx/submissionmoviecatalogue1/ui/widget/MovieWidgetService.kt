package com.cx.submissionmoviecatalogue1.ui.widget

import android.content.Intent
import android.widget.RemoteViewsService

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    2019-08-18.
 */
class MovieWidgetService : RemoteViewsService() {

    override fun onGetViewFactory(p0: Intent?): RemoteViewsFactory {
        return StackRemoteViewsFactory(this.applicationContext)
    }
}