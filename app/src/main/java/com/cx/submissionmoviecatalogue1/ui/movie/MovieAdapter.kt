package com.cx.submissionmoviecatalogue1.ui.movie

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.model.ResultsMovie
import com.cx.submissionmoviecatalogue1.utils.Const
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_list.view.*

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    24/06/19.
 */
class MovieAdapter(private val listener:(ResultsMovie)->Unit)
    : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    private val data: ArrayList<ResultsMovie> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MovieViewHolder {
        val view:View = LayoutInflater.from(p0.context).inflate(R.layout.item_list, p0, false)
        return MovieViewHolder(view)
    }

    override fun getItemCount(): Int=data.size

    override fun onBindViewHolder(p0: MovieViewHolder, p1: Int) {
        p0.bind(data[p1],listener)
    }

    fun setData(item:ArrayList<ResultsMovie>){
        data.clear()
        data.addAll(item)
        notifyDataSetChanged()
    }

    inner class MovieViewHolder(view:View):RecyclerView.ViewHolder(view){

        fun bind(movie: ResultsMovie,listener: (ResultsMovie) -> Unit) {
            itemView.tvTitle.text = movie.title
            itemView.tvEpisodes.text = movie.releaseDate
            movie.posterPath.let { Picasso.get().load("${Const.BASE_IMG_URL}$it").into(itemView.imgPoster) }
            itemView.setOnClickListener {
                listener(movie)
            }
        }
    }
}