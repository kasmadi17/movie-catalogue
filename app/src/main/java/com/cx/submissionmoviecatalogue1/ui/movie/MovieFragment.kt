package com.cx.submissionmoviecatalogue1.ui.movie

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.model.ResultsMovie
import com.cx.submissionmoviecatalogue1.ui.movie.moviedetail.DetailActivity
import com.cx.submissionmoviecatalogue1.ui.search.SearchActivity
import com.cx.submissionmoviecatalogue1.utils.Const.MOVIE
import kotlinx.android.synthetic.main.list.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.startActivity

class MovieFragment : Fragment(), AnkoLogger {

    private lateinit var adapter: MovieAdapter
    private lateinit var movieViewModel: MovieViewModel
    private lateinit var sharePref: SharePref
    private var language: String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        sharePref = SharePref(requireContext())
        language = sharePref.language
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        if (id == R.id.search) {
            activity?.startActivity<SearchActivity>("type" to 0)
        }
        return super.onOptionsItemSelected(item)
    }

    private val getMovie = Observer<ArrayList<ResultsMovie>> {
        if (it != null) {
            adapter.setData(it)
        }
    }

    private val getStatus = Observer<Boolean> {
        when (it) {
            true -> swipe.isRefreshing = true
            false -> swipe.isRefreshing = false
        }
    }

    private fun init() {
        movieViewModel = ViewModelProviders.of(requireActivity()).get(MovieViewModel::class.java)
        movieViewModel.getMovie().observe(this, getMovie)
        movieViewModel.setMovie(language.toString())
        movieViewModel.onLoad().observe(this, getStatus)

        adapter = MovieAdapter {
            context?.startActivity<DetailActivity>(MOVIE to it)
            info("id ${it.id}")
        }
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        rcView.layoutManager = llm
        rcView.adapter = adapter

        swipe.setOnRefreshListener {
            movieViewModel.setMovie(language.toString())
        }
    }
}
