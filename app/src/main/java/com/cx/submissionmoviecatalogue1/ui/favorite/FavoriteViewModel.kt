package com.cx.submissionmoviecatalogue1.ui.favorite

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    31/07/19.
 */
class FavoriteViewModel(application: Application) : AndroidViewModel(application) {
    private var listMovie = MutableLiveData<ArrayList<MovieTable>>()
    private var resultMovie: ArrayList<MovieTable> = ArrayList()
    private val movieDao = AppDatabase.getInstance(application)?.movieDao()

    fun setMovie(type:Int) {
        val result=movieDao?.getAllMovie(type)
        resultMovie.clear()
        result?.let { resultMovie.addAll(it) }
        listMovie.postValue(resultMovie)
    }

    fun getMovie(): LiveData<ArrayList<MovieTable>> {
        return listMovie
    }


}