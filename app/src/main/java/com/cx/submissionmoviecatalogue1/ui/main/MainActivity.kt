package com.cx.submissionmoviecatalogue1.ui.main

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.ui.favorite.FavoriteFragment
import com.cx.submissionmoviecatalogue1.ui.movie.MovieFragment
import com.cx.submissionmoviecatalogue1.ui.tvshow.TvShowFragment
import com.cx.submissionmoviecatalogue1.utils.Const.DEFAULT_LOCALE
import com.cx.submissionmoviecatalogue1.utils.Const.LOCALE_ENGLISH
import com.cx.submissionmoviecatalogue1.utils.Const.LOCALE_IN
import com.cx.submissionmoviecatalogue1.utils.setLanguage
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.AnkoLogger

class MainActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var sharePref: SharePref
    private lateinit var config: Configuration
    private var tag: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState != null) {
            val fragment = supportFragmentManager.findFragmentByTag(tag)
            fragment?.let { addFragment(it) }
        }else{
            bottomNavigationView.selectedItemId = R.id.movie
            supportActionBar?.title = getString(R.string.movie)
            tag = MovieFragment::class.java.simpleName
            addFragment(MovieFragment())
        }
        init()

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putString("fragment", tag)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.language_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.idn -> updateConfig(LOCALE_IN)
            R.id.english -> updateConfig(LOCALE_ENGLISH)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addFragment(fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, fragment, tag)
        fragmentTransaction.commit()
    }

    private fun updateConfig(language: String) {
        setLanguage(baseContext, language)
        sharePref.saveString(DEFAULT_LOCALE, language)
        recreate()
    }

    private fun bottomNavigation() {
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.movie -> {
                    supportActionBar?.title = getString(R.string.movie)
                    tag = MovieFragment::class.java.simpleName
                    addFragment(MovieFragment())
                }
                R.id.tvShow -> {
                    supportActionBar?.title = getString(R.string.tv_show)
                    tag = TvShowFragment::class.java.simpleName
                    addFragment(TvShowFragment())
                }
                R.id.favorite -> {
                    supportActionBar?.title = getString(R.string.favorite)
                    tag = FavoriteFragment::class.java.simpleName
                    addFragment(FavoriteFragment())
                }
            }
            true
        }

    }

    private fun init() {
        bottomNavigation()
        config = resources.configuration
        sharePref = SharePref(baseContext)
    }
}
