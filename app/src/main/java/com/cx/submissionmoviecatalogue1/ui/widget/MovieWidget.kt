package com.cx.submissionmoviecatalogue1.ui.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.RemoteViews
import android.widget.Toast
import com.cx.submissionmoviecatalogue1.R
import kotlinx.android.synthetic.main.movie_widget.view.*

/**
 * Implementation of App Widget functionality.
 */
class MovieWidget : AppWidgetProvider() {


    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {

        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if (intent?.action!=null){
            if (intent.action!=null){
                val viewIndex = intent.getIntExtra(EXTRA_ITEM,0)
                Toast.makeText(context,"touch view $viewIndex", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        val TOAST_ACTION = "com.dicoding.picodiploma.TOAST_ACTION"
        val EXTRA_ITEM = "com.dicoding.picodiploma.EXTRA_ITEM"

        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {

            val intent = Intent(context, StackWidgetService::class.java)
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            intent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))

            val views = RemoteViews(context.packageName, R.layout.movie_widget)
            views.setRemoteAdapter(R.id.stack_view, intent)
            views.setEmptyView(R.id.stack_view, R.id.empty_view)

            val toasIntent = Intent(context, MovieWidget::class.java)
            toasIntent.action = TOAST_ACTION
            toasIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            toasIntent.data = Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME))

            val pendingIntent =PendingIntent.getBroadcast(context,0,toasIntent,PendingIntent.FLAG_UPDATE_CURRENT)
            views.setPendingIntentTemplate(R.id.stack_view,pendingIntent)

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

