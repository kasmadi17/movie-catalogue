package com.cx.submissionmoviecatalogue1.ui.tvshow

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.utils.Const
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_tv_show.view.*

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    30/06/19.
 */
class TvShowAdapter(private val listener: (ResultsTvShow) -> Unit) :
    RecyclerView.Adapter<TvShowAdapter.MyViewHolder>() {

    private val tvShow: ArrayList<ResultsTvShow> = ArrayList()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val view: View = LayoutInflater.from(p0.context).inflate(R.layout.item_tv_show, p0, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int = tvShow.size

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.binData(tvShow[p1], listener)
    }

    fun setData(item: ArrayList<ResultsTvShow>) {
        tvShow.clear()
        tvShow.addAll(item)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun binData(data: ResultsTvShow, listener: (ResultsTvShow) -> Unit) {
            itemView.tvTitle.text = data.name
            itemView.tvEpisodes.text = data.firstAirDate
            data.posterPath.let { Picasso.get().load("${Const.BASE_IMG_URL}$it").into(itemView.imgPoster) }

            itemView.setOnClickListener {
                listener(data)
            }
        }
    }
}