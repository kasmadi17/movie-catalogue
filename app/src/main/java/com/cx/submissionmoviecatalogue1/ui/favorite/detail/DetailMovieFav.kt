package com.cx.submissionmoviecatalogue1.ui.favorite.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable
import com.cx.submissionmoviecatalogue1.ui.movie.moviedetail.DetailViewModel
import com.cx.submissionmoviecatalogue1.utils.Const
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_movie.*
import kotlinx.android.synthetic.main.detail_movie_content.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class DetailMovieFav : AppCompatActivity() {

    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var db: AppDatabase? = null
    private var favoriteMovie: MovieTable? = null

    private val detailViewModel: DetailViewModel by lazy {
        ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(this.application))
            .get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_movie)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.favorite -> {
                if (isFavorite)delete() else insert()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun init() {
        db = AppDatabase.getInstance(this)
        setData()
        checkFavorite()
        setFavorite()
    }

    private fun setData(){
        favoriteMovie = intent.getParcelableExtra<MovieTable>(Const.MOVIE).apply {
            Picasso.get().load("${Const.IMG_URL_W342}$poster").into(imgPoster)
            tvTitle.text = title
            tvEpisodes.text = releaseDate
            collapsing.title = title
            tvDesc.text = desc
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

    private fun checkFavorite() {

        doAsync{
            favoriteMovie?._id?.let { detailViewModel.getMovieId(it) }
            uiThread {
                val getMovie = Observer<MovieTable>{
                    isFavorite = it!=null
                }
                detailViewModel.getMovie().observe(this@DetailMovieFav,getMovie)
            }
        }
    }

    private fun insert(){
        doAsync {
            favoriteMovie?.let { detailViewModel.insert(it) }
            uiThread {
                toast(getString(R.string.msg_save_fav))
            }
        }
    }

    private fun delete(){
        doAsync {
            favoriteMovie?.let { detailViewModel.delete(it) }
            uiThread {
                toast(getString(R.string.msg_delete_fav))
            }
        }
    }
}
