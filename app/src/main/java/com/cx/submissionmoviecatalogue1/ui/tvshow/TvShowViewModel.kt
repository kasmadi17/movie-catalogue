package com.cx.submissionmoviecatalogue1.ui.tvshow

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.cx.submissionmoviecatalogue1.BuildConfig
import com.cx.submissionmoviecatalogue1.data.network.NetworkApi
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.model.TvShowModel
import org.jetbrains.anko.AnkoLogger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    04/07/19.
 */
class TvShowViewModel:ViewModel(),AnkoLogger{

    private var listTvShow= MutableLiveData<ArrayList<ResultsTvShow>>()
    private var resultTvShow:ArrayList<ResultsTvShow> = ArrayList()
    private val mOnLoad = MutableLiveData<Boolean>()

    fun setTvShow(language:String){
        mOnLoad.value = true
        val call: Call<TvShowModel> = NetworkApi().makeService().getTv(BuildConfig.API_KEY,language)
        call.enqueue(object : Callback<TvShowModel> {

            override fun onFailure(call: Call<TvShowModel>, t: Throwable) {
                mOnLoad.value = false
            }

            override fun onResponse(call: Call<TvShowModel>, response: Response<TvShowModel>) {
                if (response.isSuccessful){
                    val data = response.body()?.results
                    data?.let { resultTvShow.addAll(it) }
                    listTvShow.postValue(resultTvShow)
                    mOnLoad.postValue(false)
                }
            }

        })
    }

    fun searchTvShow(language:String,title:String){
        mOnLoad.value = true
        val call: Call<TvShowModel> = NetworkApi().makeService().searchTvShow(BuildConfig.API_KEY,language,title)
        call.enqueue(object : Callback<TvShowModel> {

            override fun onFailure(call: Call<TvShowModel>, t: Throwable) {
                mOnLoad.value = false
            }

            override fun onResponse(call: Call<TvShowModel>, response: Response<TvShowModel>) {
                if (response.isSuccessful){
                    val data = response.body()?.results
                    data?.let { resultTvShow.addAll(it) }
                    listTvShow.postValue(resultTvShow)
                    mOnLoad.postValue(false)
                }
            }

        })
    }

    fun getTvShow(): LiveData<ArrayList<ResultsTvShow>> {
        return listTvShow
    }

    fun onLoad():LiveData<Boolean>{
        return mOnLoad
    }
}