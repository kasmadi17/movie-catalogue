package com.cx.submissionmoviecatalogue1.ui.favorite.movie


import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.CONTENT_URI
import com.cx.submissionmoviecatalogue1.data.local.database.MappingHelper.mapCursorToArrayList
import com.cx.submissionmoviecatalogue1.ui.favorite.FavoriteAdapter
import com.cx.submissionmoviecatalogue1.ui.favorite.detail.DetailMovieFav
import com.cx.submissionmoviecatalogue1.utils.Const
import kotlinx.android.synthetic.main.list.*
import org.jetbrains.anko.*

class MovieFavoriteFragment : Fragment(), AnkoLogger {

    private lateinit var adapter: FavoriteAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        getMovie()
    }

    private fun init() {
        getMovie()
        adapter = FavoriteAdapter {
            activity?.startActivity<DetailMovieFav>(Const.MOVIE to it)
        }
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        rcView.layoutManager = llm
        rcView.adapter = adapter
        swipe.isEnabled = false
    }

    private fun getMovie() {
        swipe.isRefreshing = true
        doAsync {
            val uri = Uri.parse("${CONTENT_URI}/0")
            info(uri)
            val cursor = requireActivity().contentResolver.query(uri, null, null, null, null)
            uiThread {
                adapter.setData(mapCursorToArrayList(cursor))
                swipe.isRefreshing = false
            }
        }
    }
}
