package com.cx.submissionmoviecatalogue1.ui.movie

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.cx.submissionmoviecatalogue1.BuildConfig
import com.cx.submissionmoviecatalogue1.data.local.dao.MovieDao
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable
import com.cx.submissionmoviecatalogue1.data.network.NetworkApi
import com.cx.submissionmoviecatalogue1.model.MovieModel
import com.cx.submissionmoviecatalogue1.model.ResultsMovie
import org.jetbrains.anko.AnkoLogger
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    03/07/19.
 */
class MovieViewModel: ViewModel(){
    private var listMovie = MutableLiveData<ArrayList<ResultsMovie>>()
    private var resultMovie:ArrayList<ResultsMovie> = ArrayList()
    private var mOnLoad=MutableLiveData<Boolean>()

    fun setMovie(language:String){
        mOnLoad.value = true
        val call: Call<MovieModel> = NetworkApi().makeService().getMovie(BuildConfig.API_KEY,language)
        call.enqueue(object : Callback<MovieModel> {

            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                mOnLoad.value = false
            }

            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                if (response.isSuccessful){
                    val data = response.body()?.results
                    resultMovie.clear()
                    data?.let { resultMovie.addAll(it) }
                    listMovie.postValue(resultMovie)
                    mOnLoad.postValue(false)
                }
            }

        })
    }

    fun searchMovie(language:String,title:String){
        mOnLoad.value = true
        val call: Call<MovieModel> = NetworkApi().makeService().searchMovie(BuildConfig.API_KEY,language,title)
        call.enqueue(object : Callback<MovieModel> {

            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                mOnLoad.value = false
            }

            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                if (response.isSuccessful){
                    val data = response.body()?.results
                    resultMovie.clear()
                    data?.let { resultMovie.addAll(it) }
                    listMovie.postValue(resultMovie)
                    mOnLoad.postValue(false)
                }
            }

        })
    }

    fun getMovie(): LiveData<ArrayList<ResultsMovie>> {
        return listMovie
    }
    fun onLoad():LiveData<Boolean>{
        return mOnLoad
    }
}