package com.cx.submissionmoviecatalogue1.ui.setting

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.service.DailyReminder
import com.cx.submissionmoviecatalogue1.service.ReminderService
import com.cx.submissionmoviecatalogue1.utils.Const.DAILY_REMINDER
import com.cx.submissionmoviecatalogue1.utils.Const.RELEASE_REMINDER
import kotlinx.android.synthetic.main.activity_setting.*
import org.jetbrains.anko.toast
import java.util.*


class SettingActivity : AppCompatActivity() {
    private lateinit var sharePref: SharePref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        sharePref = SharePref(this)

        swDaily.isChecked = sharePref.dailyReminder
        swRelease.isChecked = sharePref.releaseReminder

        swDaily.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                sharePref.saveBoolean(DAILY_REMINDER, true)
                startJob(DailyReminder::class.java, 7, 7)
            } else {
                sharePref.saveBoolean(DAILY_REMINDER, false)
                cancelJob(7)
            }
        }

        swRelease.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                sharePref.saveBoolean(RELEASE_REMINDER, true)
                startJob(ReminderService::class.java, 8, 8)
            } else {
                sharePref.saveBoolean(RELEASE_REMINDER, false)
                cancelJob(8)
            }
        }

    }

    private fun startJob(service: Class<*>, hour: Int, jobId: Int) {
        val calender = Calendar.getInstance()
        calender.set(Calendar.HOUR_OF_DAY, hour)
        calender.set(Calendar.MINUTE, 0)
        calender.set(Calendar.SECOND, 0)
        val delay = 5000
        val mServiceComponent = ComponentName(this, service)
        val builder = JobInfo.Builder(jobId, mServiceComponent)
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
        builder.setRequiresDeviceIdle(false)
        builder.setRequiresCharging(false)
        builder.setPeriodic(calender.timeInMillis + delay)
        val jobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jobScheduler.schedule(builder.build())
        Toast.makeText(this, "Job Service started", Toast.LENGTH_SHORT).show()
    }

    private fun cancelJob(id: Int) {
        val jb: JobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        jb.cancel(id)
        toast("Notification off")
    }
}
