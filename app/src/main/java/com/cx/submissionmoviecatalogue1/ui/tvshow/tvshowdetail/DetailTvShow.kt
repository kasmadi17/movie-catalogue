package com.cx.submissionmoviecatalogue1.ui.tvshow.tvshowdetail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.ui.movie.moviedetail.DetailViewModel
import com.cx.submissionmoviecatalogue1.utils.Const.IMG_URL_W342
import com.cx.submissionmoviecatalogue1.utils.Const.TV_SHOW
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_tv_show_content.*
import kotlinx.android.synthetic.main.item_tv_show.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class DetailTvShow : AppCompatActivity() {

    private var movie: ResultsTvShow? = null
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var db: AppDatabase? = null
    private var favoriteMovie: MovieTable? = null

    private val detailViewModel: DetailViewModel by lazy {
        ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(this.application))
            .get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_tv_show)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.favorite -> {
                if (isFavorite)delete() else insert()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun init() {
        db = AppDatabase.getInstance(this)
        setData()
        checkFavorite()
        setFavorite()
    }

    private fun setData(){
        movie=intent.getParcelableExtra<ResultsTvShow>(TV_SHOW).apply {
            tvTitleShow.text = name
            tvReleaseShow.text = firstAirDate
            posterPath.let { Picasso.get().load("$IMG_URL_W342 $it").into(imgPoster) }
            if (overview.isNullOrBlank()) {
                tvDescShow.text = getString(R.string.overview_not_available)
            } else {
                tvDescShow.text = overview
            }
            favoriteMovie = MovieTable(id!!.toLong(), name, firstAirDate, posterPath, originalLanguage, overview,1)
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

    private fun checkFavorite() {

        doAsync{
            favoriteMovie?._id?.let { detailViewModel.getMovieId(it) }
            uiThread {
                val getMovie = Observer<MovieTable>{
                    isFavorite = it!=null
                }
                detailViewModel.getMovie().observe(this@DetailTvShow,getMovie)
            }
        }
    }

    private fun insert(){
        doAsync {
            favoriteMovie?.let { detailViewModel.insert(it) }
            uiThread {
                toast(getString(R.string.msg_save_fav))
            }
        }
    }

    private fun delete(){
        doAsync {
            favoriteMovie?.let { detailViewModel.delete(it) }
            uiThread {
                toast(getString(R.string.msg_delete_fav))
            }
        }
    }
}
