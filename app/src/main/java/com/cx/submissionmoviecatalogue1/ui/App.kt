package com.cx.submissionmoviecatalogue1.ui

import android.app.Application
import android.arch.persistence.room.Room
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.utils.setLanguage

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    05/07/19.
 */
class App:Application() {

    private lateinit var sharePref: SharePref

    override fun onCreate() {
        super.onCreate()
        sharePref = SharePref(baseContext)
        setLanguage(baseContext,sharePref.language.toString())
    }
}