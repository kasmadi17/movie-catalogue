package com.cx.submissionmoviecatalogue1.ui.tvshow


import android.app.SearchManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.SearchView
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.model.ResultsTvShow
import com.cx.submissionmoviecatalogue1.ui.tvshow.tvshowdetail.DetailTvShow
import com.cx.submissionmoviecatalogue1.utils.Const.TV_SHOW
import kotlinx.android.synthetic.main.list.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity

/**
 * A simple [Fragment] subclass.
 *
 */
class TvShowFragment : Fragment(), AnkoLogger {

    private lateinit var adapter: TvShowAdapter
    private lateinit var viewModel: TvShowViewModel
    private lateinit var sharePref: SharePref
    private var language: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharePref = SharePref(requireContext())
        language = sharePref.language
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_tv_show, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("language", sharePref.language)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        val searchView = menu?.findItem(R.id.search)?.actionView as SearchView
        searchView.setSearchableInfo(
            searchManager
                .getSearchableInfo(activity?.componentName)
        )
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.queryHint = getString(R.string.movie_title)
        searchView.requestFocusFromTouch()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (!query.isNullOrEmpty()) {
                    viewModel.getTvShow().observe(this@TvShowFragment, getTvShow)
                    viewModel.searchTvShow(language.toString(), query)

                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (!newText.isNullOrEmpty()) {
                    if (newText.length>3){
                        viewModel.getTvShow().observe(this@TvShowFragment, getTvShow)
                        viewModel.searchTvShow(language.toString(), newText)
                    }
                }
                return false
            }

        })

        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        return if (id == R.id.search) {
            true
        } else super.onOptionsItemSelected(item)
    }

    private val getTvShow = Observer<ArrayList<ResultsTvShow>> {
        if (it != null) {
            adapter.setData(it)
        }
    }

    private val getStatus = Observer<Boolean> {
        when (it) {
            true -> swipe.isRefreshing = true
            else -> swipe.isRefreshing = false
        }
    }

    private fun init() {
        viewModel = ViewModelProviders.of(requireActivity()).get(TvShowViewModel::class.java)
        viewModel.getTvShow().observe(this, getTvShow)
        viewModel.setTvShow(language.toString())
        viewModel.onLoad().observe(this, getStatus)
        adapter = TvShowAdapter {
            context?.startActivity<DetailTvShow>(TV_SHOW to it)
        }
        val llm = LinearLayoutManager(requireContext())
        llm.orientation = LinearLayoutManager.VERTICAL
        rcView.layoutManager = llm

        rcView.adapter = adapter
        adapter.notifyDataSetChanged()
        swipe.setOnRefreshListener {
            viewModel.setTvShow(language.toString())
        }
    }

}
