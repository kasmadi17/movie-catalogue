package com.cx.submissionmoviecatalogue1.ui.favorite

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.ui.favorite.movie.MovieFavoriteFragment
import com.cx.submissionmoviecatalogue1.ui.favorite.tvshow.TvShowFavoriteFragment
import kotlinx.android.synthetic.main.fragment_favorite.*

class FavoriteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        setHasOptionsMenu(false)
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun setUpViewPager(viewPager: ViewPager) {
        val adapter = fragmentManager?.let { ViewPagerAdapter(it) }
        adapter?.addFragment(MovieFavoriteFragment(),getString(R.string.movie))
        adapter?.addFragment(TvShowFavoriteFragment(),getString(R.string.tv_show))
        viewPager.adapter=adapter
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

}
