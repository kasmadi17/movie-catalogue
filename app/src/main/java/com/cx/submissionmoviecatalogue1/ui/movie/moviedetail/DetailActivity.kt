package com.cx.submissionmoviecatalogue1.ui.movie.moviedetail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.SharePref
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable
import com.cx.submissionmoviecatalogue1.model.ResultsMovie
import com.cx.submissionmoviecatalogue1.utils.Const.IMG_URL_W342
import com.cx.submissionmoviecatalogue1.utils.Const.MOVIE
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.detail_movie.*
import kotlinx.android.synthetic.main.detail_movie_content.*
import kotlinx.android.synthetic.main.toolbar.*
import org.jetbrains.anko.*

class DetailActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var sharePref: SharePref
    private var movie: ResultsMovie? = null
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var db: AppDatabase? = null
    private var favoriteMovie: MovieTable? = null

    private val detailViewModel: DetailViewModel by lazy {
        ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(this.application))
            .get(DetailViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_movie)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.favorite_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.favorite -> {
                if (isFavorite)delete() else insert()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun init() {
        sharePref = SharePref(this)
        db = AppDatabase.getInstance(this)
        setData()
        checkFavorite()
        setFavorite()
    }

    private fun setData(){
        movie = intent.getParcelableExtra<ResultsMovie>(MOVIE).apply {
            Picasso.get().load("$IMG_URL_W342$posterPath").into(imgPoster)
            tvTitle.text = title
            tvEpisodes.text = releaseDate
            collapsing.title = title
            if (overview.isNullOrBlank()) {
                tvDesc.text = getString(R.string.overview_not_available)
            } else {
                tvDesc.text = overview
            }
            info("id $id")
            favoriteMovie =
                id?.toLong()?.let { MovieTable(it, title, releaseDate, posterPath, originalLanguage, overview,0) }
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_black_24dp)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite_border_black_24dp)
    }

    private fun checkFavorite() {

        doAsync{
            favoriteMovie?._id?.let { detailViewModel.getMovieId(it) }
            uiThread {
                val getMovie =Observer<MovieTable>{
                    isFavorite = it!=null
                    info(favoriteMovie?._id)
                    info(isFavorite)
                    info(it?.title)
                }
                detailViewModel.getMovie().observe(this@DetailActivity,getMovie)
            }
        }
    }

    private fun insert(){
        doAsync {
            favoriteMovie?.let { detailViewModel.insert(it) }
            uiThread {
                toast(getString(R.string.msg_save_fav))
            }
        }
    }

    private fun delete(){
        doAsync {
            favoriteMovie?.let { detailViewModel.delete(it) }
            uiThread {
                toast(getString(R.string.msg_delete_fav))
            }
        }
    }
}
