package com.cx.submissionmoviecatalogue1.ui.movie.moviedetail

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    31/07/19.
 */
class DetailViewModel(application: Application):AndroidViewModel(application) {

    private var resultMovie=MutableLiveData<MovieTable>()
    private var movie:MovieTable?=null
    private val movieDao = AppDatabase.getInstance(application)?.movieDao()


    fun insert(movie:MovieTable) = movieDao?.insertMovie(movie)

    fun delete(movie: MovieTable) = movieDao?.deleteMovie(movie)

    fun getMovieId(id:Long){
        movie = movieDao?.movieById(id)
        resultMovie.postValue(movie)
    }

    fun getMovie():LiveData<MovieTable>{
        return resultMovie
    }
}