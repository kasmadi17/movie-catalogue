package com.cx.submissionmoviecatalogue1.ui.widget

import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Binder
import android.os.Bundle
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.CONTENT_URI
import com.cx.submissionmoviecatalogue1.data.local.database.MappingHelper.mapCursorToArrayList
import com.cx.submissionmoviecatalogue1.model.Movie
import com.cx.submissionmoviecatalogue1.ui.widget.MovieWidget.Companion.EXTRA_ITEM
import com.cx.submissionmoviecatalogue1.utils.Const
import com.cx.submissionmoviecatalogue1.utils.Const.BASE_IMG_URL
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.squareup.picasso.PicassoProvider
import com.squareup.picasso.Target
import kotlinx.android.synthetic.main.detail_movie.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.lang.Exception

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    08/08/19.
 */
class StackRemoteViewsFactory(val context: Context) :
    RemoteViewsService.RemoteViewsFactory,
    AnkoLogger {

    private var movieResult: MutableList<Movie> = mutableListOf()

    private lateinit var target: Target
    private var cursor: Cursor? = null

    override fun onCreate() {
//        getData()
    }

    override fun getLoadingView(): RemoteViews? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun onDataSetChanged() {
        if (cursor != null) {
            cursor?.close()
        }
        val token = Binder.clearCallingIdentity()
        cursor = context.contentResolver.query(CONTENT_URI, null, null, null, null)
        movieResult = mapCursorToArrayList(cursor!!)
        Binder.restoreCallingIdentity(token)
    }

    override fun hasStableIds(): Boolean = false

    override fun getViewAt(position: Int): RemoteViews {
        info("movie result size $BASE_IMG_URL ${movieResult[position].poster}")

        val rv = RemoteViews(context.packageName, R.layout.widget_item)
        val bitmap =Picasso.get().load("$BASE_IMG_URL${movieResult[position].poster}").get()
        rv.setImageViewBitmap(R.id.imageView,bitmap)

        val bundle = Bundle()
        bundle.putInt(EXTRA_ITEM, position)
        val intent = Intent()
        intent.putExtras(bundle)
        rv.setOnClickFillInIntent(R.id.imageView, intent)
        return rv
    }


    override fun getCount(): Int = movieResult.size

    override fun getViewTypeCount(): Int = 3

    override fun onDestroy() {
        info("destroy widget")
    }
}