package com.cx.submissionmoviecatalogue1.ui.favorite.tvshow


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.cx.submissionmoviecatalogue1.R
import com.cx.submissionmoviecatalogue1.data.local.database.AppDatabase
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable
import com.cx.submissionmoviecatalogue1.ui.favorite.FavoriteAdapter
import com.cx.submissionmoviecatalogue1.ui.favorite.FavoriteViewModel
import com.cx.submissionmoviecatalogue1.ui.favorite.detail.DetailMovieFav
import com.cx.submissionmoviecatalogue1.utils.Const
import kotlinx.android.synthetic.main.list.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.uiThread

class TvShowFavoriteFragment : Fragment() {

    private lateinit var adapter: FavoriteAdapter
    private var db: AppDatabase? = null
    private val favViewModel: FavoriteViewModel by lazy {
        ViewModelProviders.of(this, ViewModelProvider.AndroidViewModelFactory(requireActivity().application))
            .get(FavoriteViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_show_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        getMovie()
    }

    private fun init() {
        db = AppDatabase.getInstance(requireContext())
        getMovie()
        adapter = FavoriteAdapter {
            activity?.startActivity<DetailMovieFav>(Const.MOVIE to it)
        }
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        rcView.layoutManager = llm
        rcView.adapter = adapter
        swipe.isEnabled = false
    }

    private fun getMovie() {
        swipe.isRefreshing = true
        doAsync {
            favViewModel.setMovie(1)
            val movie = Observer<ArrayList<MovieTable>> { it1 ->
                uiThread {
                    it1?.let { it2 -> adapter.setData(it2) }
                    swipe.isRefreshing = false
                }
            }
            favViewModel.getMovie().observe(requireActivity(), movie)
        }
    }
}

