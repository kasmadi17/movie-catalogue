package com.cx.submissionmoviecatalogue1.data.local.database

import android.net.Uri
import android.provider.BaseColumns

/**
 * @author kasmadi <kasmadi1717></kasmadi1717>@gmail.com>
 * @version 1
 * @since 2019-08-24.
 */
object DatabaseContract {

    const val AUTHORITY = "com.cx.submissionmoviecatalogue1"
    private const val SCHEME = "content"

    class MovieColumns : BaseColumns {
        companion object {
            const val TABLE_NAME = "movie"
            const val IDS = "ids"
            const val TITLE = "title"
            const val DESCRIPTION = "description"
            const val RELEASE_DATE = "release_date"
            const val POSTER = "poster"
            const val TYPE = "type"

            val CONTENT_URI: Uri = Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build()
        }
    }
}
