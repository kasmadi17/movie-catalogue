package com.cx.submissionmoviecatalogue1.data.network

import com.cx.submissionmoviecatalogue1.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    03/07/19.
 */
class NetworkApi {

    fun makeService():NetworkService {
        val httpClient = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(logging)
        }

        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(NetworkService::class.java)
    }
}