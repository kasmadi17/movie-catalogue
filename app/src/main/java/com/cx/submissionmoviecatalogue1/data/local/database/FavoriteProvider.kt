package com.cx.submissionmoviecatalogue1.data.local.database

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.os.Build
import com.cx.submissionmoviecatalogue1.BuildConfig


/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    07/08/19.
 */
class FavoriteProvider : ContentProvider() {

    private val AUTHORITY = BuildConfig.APPLICATION_ID

    private val uriMatcher: UriMatcher = UriMatcher(UriMatcher.NO_MATCH)
    private var db = AppDatabase.getInstance(context)
    private val BASE_PATH = "movietable"
    private val CONTENT_URI = Uri.parse("content://$AUTHORITY/$BASE_PATH")
    private val MOVIE = 1
    private val TV_SHOW = 2
    private val MOVIE_ID = 3

    init {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, MOVIE)
        uriMatcher.addURI(AUTHORITY, BASE_PATH, TV_SHOW)
        uriMatcher.addURI(AUTHORITY, "$BASE_PATH/#", MOVIE_ID)
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
//        val added:Long? = when (uriMatcher.match(uri)){
//            MOVIE-> db?.movieDao()?.insertMovieProvider(values)
//            else-> 0
//        }
        //        context!!.contentResolver.notifyChange(CONTENT_URI, MainActivity.DataObserver(Handler(), context))
        return Uri.parse("$CONTENT_URI/")
    }

    override fun query(
        uri: Uri,
        projection: Array<String>?,
        selection: String?,
        selectionArgs: Array<String>?,
        sortOrder: String?
    ): Cursor? {
        val cursor: Cursor?
        when (uriMatcher.match(uri)) {
//            MOVIE -> cursor= db?.movieDao()?.getAllMovieProvider(MOVIE)
//            TV_SHOW -> cursor = db?.movieDao()?.getAllMovieProvider(TV_SHOW)
//            MOVIE_ID -> cursor = db?.movieDao()?.movieByIdProvider(uri.lastPathSegment)
            else -> cursor = null
        }
        return cursor
    }

    override fun onCreate(): Boolean {
        return true
    }

    override fun update(uri: Uri, values: ContentValues?, selection: String?, selectionArgs: Array<String>?): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val deleted:Int = when(uriMatcher.match(uri)){
//            MOVIE_ID-> db?.movieDao()?.deleteMovieProvider(uri.lastPathSegment)!!
            else -> 0
        }
        //getContext().getContentResolver().notifyChange(CONTENT_URI, new MainActivity.DataObserver(new Handler(), getContext()));
        return deleted
    }

    override fun getType(uri: Uri): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}