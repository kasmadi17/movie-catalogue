package com.cx.submissionmoviecatalogue1.data.local.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.cx.submissionmoviecatalogue1.data.local.dao.MovieDao

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    31/07/19.
 */
@Database(entities = [MovieTable::class], version = 1)
abstract class AppDatabase:RoomDatabase() {

    abstract fun movieDao():MovieDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "moviecatalogue.db")
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}