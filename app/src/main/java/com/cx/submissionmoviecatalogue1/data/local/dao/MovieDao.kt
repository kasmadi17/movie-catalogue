package com.cx.submissionmoviecatalogue1.data.local.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.content.ContentValues
import android.database.Cursor
import com.cx.submissionmoviecatalogue1.data.local.database.MovieTable


/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    31/07/19.
 */
@Dao
interface MovieDao {

    @Query("select * from movietable where _id =:id")
    fun movieById(id:Long):MovieTable

    @Query("select * from movietable where language = :language")
    fun getMovieByLanguage(language:String):List<MovieTable>

    @Query("select * from movietable where type = :type")
    fun getAllMovie(type:Int):List<MovieTable>

    @Delete
    fun deleteMovie(vararg movie:MovieTable)

    @Insert
    fun insertMovie(vararg movie:MovieTable)

//    @Query("select * from movietable where _id =:id")
//    fun movieByIdProvider(id:String?):Cursor
//
//    @Query("select * from movietable where type = :type")
//    fun getAllMovieProvider(type:Int):Cursor
//
//    @Insert
//    fun insertMovieProvider(vararg movie:ContentValues?):Long
//
//    @Query("DELETE FROM movietable WHERE _id =:id")
//    fun deleteMovieProvider(id:String?):Int

}