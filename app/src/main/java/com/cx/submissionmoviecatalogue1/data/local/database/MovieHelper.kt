package com.cx.submissionmoviecatalogue1.data.local.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns._ID
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.DESCRIPTION
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.IDS
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.POSTER
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.RELEASE_DATE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TABLE_NAME
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TITLE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TYPE
import com.cx.submissionmoviecatalogue1.model.Movie
import java.util.*

/**
 * @author kasmadi <kasmadi1717></kasmadi1717>@gmail.com>
 * @version 1
 * @since 2019-08-24.
 */
class MovieHelper private constructor(context: Context) {
    private val dataBaseHelper: DatabaseHelper = DatabaseHelper(context)

    private var database: SQLiteDatabase? = null


    @Throws(SQLException::class)
    fun open() {
        database = dataBaseHelper.writableDatabase
    }

    fun close() {
        dataBaseHelper.close()

        if (database!!.isOpen)
            database!!.close()
    }

    fun query(): ArrayList<Movie> {
        val arrayList = ArrayList<Movie>()
        val cursor =
            database!!.query(DATABASE_TABLE, null, null, null, null, null, "$_ID DESC", null)
        cursor.moveToFirst()
        var movie: Movie
        if (cursor.count > 0) {
            do {
                movie = Movie(
                    cursor.getInt(cursor.getColumnIndexOrThrow(IDS)),
                    cursor.getString(cursor.getColumnIndexOrThrow(TITLE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION)),
                    cursor.getString(cursor.getColumnIndexOrThrow(RELEASE_DATE)),
                    cursor.getString(cursor.getColumnIndexOrThrow(POSTER)),
                    cursor.getInt(cursor.getColumnIndexOrThrow(TYPE))
                )

                arrayList.add(movie)
                cursor.moveToNext()

            } while (!cursor.isAfterLast)
        }
        cursor.close()
        return arrayList
    }

    fun queryByIdProvider(id: String): Cursor {
        return database!!.query(
            DATABASE_TABLE,
            null,
            "$IDS = ?",
            arrayOf(id),
            null,
            null,
            null,
            null
        )
    }

    fun queryProvider(type: String): Cursor {
        return database!!.query(
            DATABASE_TABLE,
            null,
            "$TYPE = ?",
            arrayOf(type),
            null,
            null,
            "$_ID ASC"
        )
    }

    fun queryProviderAll(): Cursor {
        return database!!.query(
            DATABASE_TABLE,
            null,
            null,
            null,
            null,
            null,
            "$_ID ASC"
        )
    }

    fun insertProvider(values: ContentValues): Long {
        return database!!.insert(DATABASE_TABLE, null, values)
    }

    fun deleteProvider(id: String): Int {
        return database!!.delete(
            DATABASE_TABLE,
            "$IDS = ?",
            arrayOf(id)
        )
    }

    companion object {

        private const val DATABASE_TABLE = TABLE_NAME
        private var INSTANCE: MovieHelper? = null

        fun getInstance(context: Context): MovieHelper? {
            if (INSTANCE == null) {
                synchronized(SQLiteOpenHelper::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = MovieHelper(context)
                    }
                }
            }
            return INSTANCE
        }
    }
}
