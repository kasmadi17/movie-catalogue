package com.cx.submissionmoviecatalogue1.data.local.database

import android.database.Cursor
import android.provider.BaseColumns._ID
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.DESCRIPTION
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.IDS
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.POSTER
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.RELEASE_DATE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TITLE
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TYPE
import com.cx.submissionmoviecatalogue1.model.Movie
import java.util.*

/**
 * @author kasmadi <kasmadi1717></kasmadi1717>@gmail.com>
 * @version 1
 * @since 2019-09-01.
 */
object MappingHelper {
    fun mapCursorToArrayList(movieCursor: Cursor): ArrayList<Movie> {
        val movieList = ArrayList<Movie>()
        while (movieCursor.moveToNext()) {
            val id = movieCursor.getInt(movieCursor.getColumnIndexOrThrow(IDS))
            val title = movieCursor.getString(movieCursor.getColumnIndexOrThrow(TITLE))
            val description = movieCursor.getString(movieCursor.getColumnIndexOrThrow(DESCRIPTION))
            val date = movieCursor.getString(movieCursor.getColumnIndexOrThrow(RELEASE_DATE))
            val poster = movieCursor.getString(movieCursor.getColumnIndexOrThrow(POSTER))
            val type = movieCursor.getInt(movieCursor.getColumnIndexOrThrow(TYPE))
            movieList.add(Movie(id, title, description, date, poster, type))
        }
        return movieList
    }
}
