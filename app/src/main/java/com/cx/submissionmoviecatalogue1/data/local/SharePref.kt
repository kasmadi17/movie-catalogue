package com.cx.submissionmoviecatalogue1.data.local

import android.content.Context
import android.content.SharedPreferences
import com.cx.submissionmoviecatalogue1.utils.Const.DAILY_REMINDER
import com.cx.submissionmoviecatalogue1.utils.Const.DEFAULT_LOCALE
import com.cx.submissionmoviecatalogue1.utils.Const.LOCALE_ENGLISH
import com.cx.submissionmoviecatalogue1.utils.Const.RELEASE_REMINDER

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    30/06/19.
 */
class SharePref(context: Context) {
    private val sp: SharedPreferences =
        context.getSharedPreferences("MovieCatalogue", Context.MODE_PRIVATE)
    private val spEditor: SharedPreferences.Editor

    init {
        spEditor = sp.edit()
    }

    val language: String?
        get() = sp.getString(DEFAULT_LOCALE, LOCALE_ENGLISH)

    val dailyReminder: Boolean
        get() = sp.getBoolean(DAILY_REMINDER, false)

    val releaseReminder: Boolean
        get() = sp.getBoolean(RELEASE_REMINDER, false)

    fun saveBoolean(key: String, value: Boolean) {
        spEditor.putBoolean(key, value)
        spEditor.commit()
    }

    fun saveString(key: String, value: String) {
        spEditor.putString(key, value)
        spEditor.commit()
    }
}