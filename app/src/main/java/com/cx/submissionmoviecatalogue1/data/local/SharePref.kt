package com.cx.submissionmoviecatalogue1.data.local

import android.content.Context
import android.content.SharedPreferences
import com.cx.submissionmoviecatalogue1.utils.Const.DEFAULT_LOCALE
import com.cx.submissionmoviecatalogue1.utils.Const.LOCALE_ENGLISH

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    30/06/19.
 */
class SharePref(context: Context) {
    private val sp: SharedPreferences = context.getSharedPreferences("MovieCatalogue", Context.MODE_PRIVATE)
    private val spEditor: SharedPreferences.Editor

    init {
        spEditor = sp.edit()
    }

    val language: String?
        get() = sp.getString(DEFAULT_LOCALE, LOCALE_ENGLISH)

    fun saveBoolean(key:String,value:Boolean) {
        spEditor.putBoolean(key, value)
        spEditor.commit()
    }

    fun saveString(key:String,value:String){
        spEditor.putString(key, value)
        spEditor.commit()
    }
}