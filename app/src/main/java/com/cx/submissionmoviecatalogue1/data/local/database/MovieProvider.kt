package com.cx.submissionmoviecatalogue1.data.local.database

import android.content.ContentProvider
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.AUTHORITY
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.CONTENT_URI
import com.cx.submissionmoviecatalogue1.data.local.database.DatabaseContract.MovieColumns.Companion.TABLE_NAME

/**
 * @author kasmadi <kasmadi1717></kasmadi1717>@gmail.com>
 * @version 1
 * @since 2019-08-24.
 */
class MovieProvider : ContentProvider() {
    private var movieHelper: MovieHelper? = null

    override fun onCreate(): Boolean {
        movieHelper = MovieHelper.getInstance(context!!)
        return true
    }

    override fun query(
        uri: Uri,
        strings: Array<String>?,
        s: String?,
        strings1: Array<String>?,
        s1: String?
    ): Cursor? {
        movieHelper!!.open()
        return when (sUriMatcher.match(uri)) {
            MOVIE -> movieHelper!!.queryProviderAll()
            MOVIE_ID -> if (uri.lastPathSegment == "0" || uri.lastPathSegment == "1") {
                movieHelper!!.queryProvider(uri.lastPathSegment!!)
            } else {
                movieHelper!!.queryByIdProvider(uri.lastPathSegment!!)
            }
            else -> null
        }
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, contentValues: ContentValues?): Uri? {
        movieHelper!!.open()
        val added: Long = when (sUriMatcher.match(uri)) {
            MOVIE -> movieHelper!!.insertProvider(contentValues!!)
            else -> 0
        }
        context!!.contentResolver.notifyChange(CONTENT_URI, null)
        return Uri.parse("$CONTENT_URI / $added")
    }

    override fun delete(uri: Uri, s: String?, strings: Array<String>?): Int {
        movieHelper!!.open()
        var deleted = 1
        when (sUriMatcher.match(uri)) {
            MOVIE_ID -> movieHelper!!.deleteProvider(uri.lastPathSegment!!)
            else -> deleted = 0
        }
        context!!.contentResolver.notifyChange(uri, null)
        return deleted
    }

    override fun update(
        uri: Uri,
        contentValues: ContentValues?,
        s: String?,
        strings: Array<String>?
    ): Int {
        return 0
    }

    companion object {

        private const val MOVIE = 0
        private const val MOVIE_ID = 1

        private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            sUriMatcher.addURI(AUTHORITY, TABLE_NAME, MOVIE)
            sUriMatcher.addURI(AUTHORITY, "$TABLE_NAME/#", MOVIE_ID)
        }
    }
}
