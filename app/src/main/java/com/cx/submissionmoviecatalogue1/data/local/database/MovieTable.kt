package com.cx.submissionmoviecatalogue1.data.local.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    31/07/19.
 */
@Parcelize
@Entity
class MovieTable(@PrimaryKey(autoGenerate = false) var _id:Long=0,
                 @ColumnInfo(name = "title") var title:String?=null,
                 @ColumnInfo(name = "release_date") var releaseDate:String?=null,
                 @ColumnInfo(name = "poster") var poster:String?=null,
                 @ColumnInfo(name = "language") var language:String?=null,
                 @ColumnInfo(name = "descriptiom") var desc:String?=null,
                 @ColumnInfo(name = "type")var type:Int=0 ):Parcelable