package com.cx.submissionmoviecatalogue1.data.network

import com.cx.submissionmoviecatalogue1.model.MovieModel
import com.cx.submissionmoviecatalogue1.model.MovieTodayModel
import com.cx.submissionmoviecatalogue1.model.TvShowModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author   kasmadi <kasmadi1717@gmail.com>
 * @version  1
 * @since    03/07/19.
 */
interface NetworkService {
    @GET("discover/movie")
    fun getMovie(
        @Query("api_key") key: String,
        @Query("language") language: String
    )
            : Call<MovieModel>

    @GET("discover/tv")
    fun getTv(
        @Query("api_key") key: String,
        @Query("language") language: String
    ): Call<TvShowModel>

    @GET("search/movie")
    fun searchMovie(
        @Query("api_key") key: String,
        @Query("language") language: String,
        @Query("query") title: String
    ): Call<MovieModel>

    @GET("search/tv")
    fun searchTvShow(
        @Query("api_key") key: String,
        @Query("language") language: String,
        @Query("query") title: String
    ): Call<TvShowModel>

    @GET("discover/movie")
    fun getMovieRelease(
        @Query("api_key") key: String,
        @Query("primary_release_date.gte") todayGte: String,
        @Query("primary_release_date.lte") today: String
    ): Call<MovieTodayModel>
}